{{- range . -}}
{{ if ne (len .Vulnerabilities) 0 }}
# Trivy config scan results
| Library | Vulnerability ID | Severity | Installed Version | Description |
| --- | --- | --- | --- | --- |
{{- range .Vulnerabilities }}
| {{ .PkgName }} | {{ .VulnerabilityID }} | {{ .Severity }} | {{ .InstalledVersion }} | {{ .Description }} |
{{- end }}
{{- end }}
{{- end }}
